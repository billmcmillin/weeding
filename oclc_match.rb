#oclc_match.rb
require 'csv'

#remove quotes from csv
quotedFile = File.read("holdings.csv")
unquotedFile = quotedFile.gsub /"/, ''
outFile = open("holdings.csv", "wb")
outFile.write(unquotedFile)
outFile.close

blacklist = []
CSV.foreach('blacklist.csv', col_sep: '|') do |row|
	blacklist << row[0]
end

holdings = CSV.read('holdings.csv', { :col_sep => '|' })
holdings.shift

send_items = []
dont_send_items = []
error_items = []

for i in 0...holdings.length
	oclc_num = holdings[i][0]
	item = holdings[i]
	if(blacklist.include?(oclc_num))
		puts "found " + oclc_num.to_s
		dont_send_items << item
	elsif oclc_num.nil? || oclc_num.empty?
		puts "error at line " + i.to_s
		error_items << item
	else
		send_items << item
	end
end

CSV.open("./send_to_sword.csv", "wb") do |csv_out|
	send_items.each do |row_array|
		csv_out << row_array
	end
end

CSV.open("./dont_send_to_sword.csv", "wb") do |csv_out|
	dont_send_items.each do |row_array|
		csv_out << row_array
	end
end	

CSV.open("./errors.csv", "wb") do |csv_out|
	error_items.each do |row_array|
		csv_out << row_array
	end
end

